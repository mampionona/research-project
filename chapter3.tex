\chapter{Dimensional regularisation}\label{chap3}
In a perturbative theory, higher-order corrections involve divergent integrals. These types of results are not in agreement with experimental observations. In 1972, Veltman and 't Hooft \citep{'tHooft:1972fi} introduced the idea of dimensional regularisation, which was based on the observation that ultraviolet divergences\footnote{In physics an ultraviolet divergence appears when an integral from the Feynman diagram diverges because of the contribution of very high energy objects (the early case was the ``ultraviolet catastrophe" of blackbody radiation). } converge for lower dimensions and 
infrared divergences\footnote{We have an infrared divergence from objects due to very low energy contributions to the theory. These only appear in cases of massless particle theories. } converge for dimensions higher than 4. These principles gave rise to the idea of analytic continuation in spacetime dimensions.
\section{Feynman parameters}
In our computations we shall encounter an integral of the form \begin{align}
\int \frac{ d^4 k [\cdots] }{A_1(k) A_2(k) \cdots A_n(k)},
\end{align} 
which is not easy to solve. To solve these integrals one has to make use of the sophisticated method of Feynman parameters.
In order to employ this method, we need to prove \emph{Feynman's formula},\begin{align}
\frac{1}{A_1 \cdots A_n} =   \int_{0}^{1}  dx_1 \cdots dx_n \delta\bigg( \sum_{i=1}^n x_i -1  \bigg) \frac{(n-1)!}{[x_1 A_1 + \cdots + x_nA_n]^n}. 
\label{FP} 
\end{align}
%\iint_0^1 dx_1 dx$
Let us prove this by induction. Considering first the simplest case:
\begin{align}
\frac{1}{A_1 A_2} &=\int_{0}^{1}  dx_1 dx_2 \delta \left(x_1 + x_2 -1  \right)\frac{1}{[x_1 A_1 + x_2 A_2]^2},\nonumber \\ 
&= \int_0^1 dx_1 \frac{1}{[x_1A_1 + (1-x_1) A_2]^2}.
\end{align}
Let us make a change of variable $u=x_1A_1 + (1-x_1) A_2$, then we get $ du = (A_1-A_2) dx_1, $ where the integration domain becomes $ x_1=0 \longrightarrow u=A_2$ and $x_1=1 \longrightarrow u=A_1$.
By plugging this into the above integral, we see that \begin{align}
\frac{1}{A_1 A_2}= \int_{A_2}^{A_1} \frac{du}{(A_1 - A_2)} \frac{1}{u^2}=\frac{1}{(A_1 - A_2)}\bigg(- \frac{1}{u} \bigg)\bigg\vert_{A_2}^{A_1} = \frac{1}{(A_1 - A_2)} \bigg( \frac{1}{A_1}- \frac{1}{A_2} \bigg).
\end{align}
Therefore we have \begin{align}
\int_{0}^{1}  dx_1 dx_2 \delta \bigg(x_1 + x_2 -1  \bigg)\frac{1}{[x_1 A_1 + x_2 A_2]^2}= \frac{1}{A_1 A_2}.
\label{feypar1}
\end{align}

By differentiating \eqref{feypar1} with respect to $A_2$,  we can prove by induction that  \begin{align}
\frac{1}{A_1 A_2^n} = \int_{0}^1 dx_1 dx_2 \delta \left(x_1 + x_2- 1\right) \frac{nx_2^{n-1}}{[x_1A_1 + x_2A_2]^{n+1}}.
\label{toprove}
\end{align}

We have shown that for $ n=1$ Equation \eqref{toprove} is true. Now let us assume that for $n \geq 1$ \eqref{toprove} holds. Let us prove that it is still true for $n+1$.

Let us differentiate both sides of \eqref{toprove} with respect to $A_2$. We will get\begin{align}
-n \frac{1}{A_1 A_2^{n+1}} = - \int_{0}^{1} dx_1 dx_2 \delta \left(x_1 + x_2 -1\right) \frac{n(n+1) x_2^{n-1} x_2}{[x_1A_1 + x_2 A_2]^{n+2}} ,\nonumber \\
\therefore \frac{1}{A_1 A_2^{n+1}} =\int_{0}^{1} dx_1 dx_2 \delta \left(x_1 + x_2 -1\right) \frac{n(n+1) x_2^{n}}{[x_1A_1 + x_2 A_2]^{n+2}}.
\end{align}

But we need a more general form of Feynman's formula. We need an expression like $\frac{1}{A_1 \cdots A_n} $. 
For that let us assume that for $n=m \geq 2$, Feynman's formula is still true. Let us show then that this is valid for $m+1$. 
Let us assume then \begin{align}
\frac{1}{A_1 \cdots A_{m}}=\int_{0}^1 dx_1 \cdots   \int_0^1 dx_n \delta\bigg(\sum_{i=1}^{m+1} x_i -1  \bigg) \frac{(n-1)!}{A_{n+1} \bigg( \sum_{i=1}^m x_i A_i \bigg) },
\end{align}
and noticing that for $n=m+1$ we have
\begin{align}
\frac{1}{A_1 \cdots A_{m+1}} &= \bigg( \prod_{i=1}^{m} A_i \bigg)^{-1} (A_{m+1})^{-1} ,\nonumber \\
&=\int_{0}^1 dx_1 \cdots   \int_0^1 dx_m \delta \bigg(\sum_{i=1}^{m} x_i -1  \bigg) \frac{(m-1)!}{A_{m+1} \bigg( \sum_{i=1}^m x_i A_i \bigg) }.
\end{align}
We can now use \eqref{toprove} in the above relation and obtain
\begin{align}
\frac{1}{A_1 \cdots A_{m+1}} &= \int_{0}^1 dx_1 \cdots   \int_0^1 dx_m \delta\bigg(\sum_{i=1}^{m} x_i -1  \bigg) (m-1)!\nonumber \\ &\hspace{.125in} \times \int_0^1 dx \int_0^1 dy \delta\bigg(x-y-1\bigg) \frac{m y^{m-1}}{\left[ xA_{m+1} + y \sum_{i=1}^m x_i A_i \right]^{m+1}}, \nonumber \\
&=  \int_{0}^1 dx_1 \cdots   \int_0^1 dx_m \delta\bigg(\sum_{i=1}^{m} x_i -1  \bigg)(m-1)! \int_0^1 dy \frac{m y^{m-1}}{\left[ (1-y)A_{m+1} + y \sum_{i=1}^m x_i A_i \right]^{m+1}}.
\end{align} 
Now we can make a change of variable $ u_i = y x_i ~~ \forall i \in \{1,\cdots, m\}$, which will give us
\begin{align}
\frac{1}{A_1 \cdots A_{m+1}} &= \int_0^1 dy \int_0^y \frac{du_1}{y}\cdots \int_0^y \frac{du_m}{y} \delta\bigg(\sum_{i=1}^{m} \frac{u_i}{y} -1  \bigg)(m-1)!\frac{m y^{m-1}}{\left[ (1-y)A_{m+1} + \sum_{i=1}^m u_i A_i \right]^{m+1}}.
\end{align} 
But we have the following property  \begin{align}
\delta\left(\sum_{i=1}^{m} \frac{u_i}{y} -1  \right)=y \delta \bigg(\sum_{i=1}^{m} u_i -y \bigg),
\end{align}
thus we obtain
\begin{align}
\frac{1}{A_1 \cdots A_{m+1}} &= \int_0^1 dy \int_0^y du_1 \cdots \int_0^y du_m \delta\bigg( \sum_{i=1}^{m} u_i -y  \bigg) \frac{m!}{[(1-y)A_{m+1} + \sum_{i=1}^m u_i A_i]^{m+1}}.
\end{align}
If we substitute $u_{m+1}= (1-y) $, we get \begin{align}
\frac{1}{A_1 \cdots A_{m+1}} &= \int_0^1 du_{m+1} \int_0^{1-u_{m+1}} du_1 \cdots \int_0^{1-u_{m+1}} du_m \delta \bigg( \sum_{i=1}^{m+1} u_i -1  \bigg) \frac{m!}{[ \sum_{i=1}^{m+1} u_i A_i]^{m+1}}.
\end{align}
Note that due to the $\delta-$functional within the integral (also $u_{m+1}$ is always positive), and if we integrate over a larger interval, the extra part does not contribute.
Therefore we can write the integral above as \begin{align}
\frac{1}{A_1 \cdots A_{m+1}} &= \int_0^{1} du_1 \cdots \int_0^{1} du_{m+1} \delta\bigg(\sum_{i=1}^{m+1} u_i -1  \bigg) \frac{m!}{[ \sum_{i=1}^{m+1} u_i A_i]^{m+1}}.
\end{align}
Finally, we see that for all $n\geq 2$, \begin{align}
\frac{1}{A_1 \cdots A_{n}} = \int_0^{1} dx_1 \cdots \int_0^{1} dx_n \delta\bigg(\sum_{i=1}^{n} x_i -1  \bigg) \frac{(n-1)!}{[ \sum_{i=1}^{n} x_i A_i]^{n}}.
\end{align}





%\frac{1}{A_1 \cdots A_{m+1}} &= \int_0^1 dx_1 \cdots \int_0^1 dx_n \delta(\sum_{i=1}^{m+1})


















%\section{Loop integrals and dimensional regularization}

\section{Loop integrals}
To start this section let us compute the area of a unit sphere in $d$-dimensions. In order to accomplish that,
%\subsection{Area of a unit sphere in $d$-dimensions}
let us now consider the Gaussian integral which is given by the relation\begin{align}
\sqrt{\pi} = \int dx e^{-x^2}. 
\end{align}

This still holds in $d$-dimensions,\begin{align}
(\sqrt{\pi} )^d &= \left( \int dx e^{-x^2} \right)^d= \int d^dx e^{\left(-\sum_{i=1}^d x_i^2 \right)},\nonumber\\
&=\int d\Omega_d \int_0^\infty  dx x^{d-1} e^{-x^2} ,\\
&= \left( \int d\Omega_d \right) \int d(x^2) (x^2)^{\frac{d}{2}-1} x e^{-x^2}.
\end{align}

We can make a change of variable $u=x^2$, which implies $du=2xdx.$ Therefore, the integral can be written as
\begin{align}
\left( \int d\Omega_d \right) \int xdx (x^2)^{\frac{d}{2}-1} e^{-x^2} &=\frac{1}{2} \left( \int d\Omega_d \right) \int du u^{\frac{d}{2}-1} e^{-u},\nonumber \\
&=\frac{1}{2} \left( \int d\Omega_d \right) \Gamma(d/2).
\end{align}

Thus the area of a unit sphere in $d$-dimensions is \begin{align}
(\sqrt{\pi} )^d = \frac{1}{2} \left( \int d\Omega_d \right) \Gamma(d/2) \Rightarrow \left( \int d\Omega_d \right) = \frac{2 \pi^{d/2} }{\Gamma	(d/2)}.
\end{align}
%For $d$ equals to 1 to 4, we have
%\begin{center}
%\begin{tabular}{|c|c|c|}
%\hline 
%$d$ & $\Gamma(d/2)$  & $\int d\Omega_d$ \\ 
%\hline 
%1 & $\sqrt{\pi}$ & 2 \\ 
%\hline 
%2 & 1 & 2$\pi$ \\ 
%\hline 
%3 & $\sqrt{\pi}/2$ & 4$\pi$ \\ 
%\hline 
%4 & 1 & 2$\pi^2$ \\ 
%\hline 
%\end{tabular}
%\end{center}

%\subsection{Dimensional regularisation}
Let us apply the result above so that we can compute an integral of the form  \begin{align}
I_d(m)=\int \frac{d^d l}{(2\pi)^{d}} \frac{1}{[l^2-\Delta]^m}.
\label{int1}
\end{align}
To evaluate this we need to consider the contour of integration in the $l^0$ plane. The pole in the denominator is at $l^0=\pm\sqrt{|l|^2 + \Delta} $.

Our integral is not well defined, so we need to use the $i\epsilon$ prescription of Feynman to push the poles, and use Wick rotation (which consists of rotating $l^0$ by $90^{\circ}$  anticlockwise). For that, we define an Euclidean four-momentum variable $l_E$ such that
\begin{align}
l^0 = i l^0_E,~~ l=l_E.
\end{align}

\begin{figure}[h!]\center
\includegraphics[scale=0.35]{Wick} 
\caption{The Wick rotation}
\end{figure}



The rotated contour goes now from $l_E^0 = -\infty $ to $\infty$. 
Let us now compute $\eqref{int1}.$

\begin{align}
\int \frac{d^d l}{(2\pi)^d} \frac{1}{[l^2-\Delta]^m}&= \frac{i (-1)^m}{(2\pi)^d} \int  \frac{d^d l_E}{[l_E^2 +\Delta]^m} \nonumber\\
&= \frac{i (-1)^m}{(2\pi)^d} \int d \Omega_d \int_{0}^\infty \frac{l_E^{d-1} dl_E}{[l_E^2 +\Delta]^m}\nonumber \\
&= \frac{i (-1)^m}{(2\pi)^d} \frac{2 \pi^{d/2} }{\Gamma	(d/2)} \frac{1}{2} \int_0^{\infty} \frac{d(l_E^2) (l_E^2)^{d/2 -1}}{[l_E^2+\Delta]^m}\nonumber\\
&= \frac{i (-1)^m}{(4\pi)^{d/2}} \frac{1 }{\Gamma	(d/2)}  \int_0^{\infty} \frac{d(l_E^2) (l_E^2)^{d/2 -1}}{[l_E^2+\Delta]^m}.
\end{align}
Let us define \begin{align}
x= \frac{\Delta}{l_E^2 + \Delta},
\end{align}
which will give us\begin{align}
l_E^2 + \Delta = \frac{\Delta}{x} \Rightarrow d(l_E^2) = -\frac{\Delta }{x^2} dx.
\end{align}
Thus, \begin{align}
I_d(m)&= \frac{i (-1)^m}{(4\pi)^{d/2}} \frac{1 }{\Gamma	(d/2)}  \int_{0}^{1} dx  \frac{\Delta }{x^2} \Delta^{d/2 -1} \left( \frac{1}{x}-1  \right)^{d/2-1}  \left(  \frac{\Delta}{x} \right)^{-m}\nonumber \\
&= \frac{i (-1)^m}{(4\pi)^{d/2}} \frac{1 }{\Gamma	(d/2)} \Delta^{d/2 -m} \int_{0}^{1} dx x^{m-d/2-1}(1-x)^{d/2-1}.
\end{align}

By definition, the beta function is \begin{align}
B(\alpha,\beta)= \int_{0}^1 dx x^{a-1} (1-x)^{b-1} = \frac{\Gamma(\alpha) \Gamma(\beta)	}{\Gamma(\alpha+\beta)},
\end{align}
therefore \begin{align}
I_d(m)&=\frac{i (-1)^m}{(4\pi)^{d/2}} \frac{\Gamma(m-d/2)}{\Gamma(m)} \Delta^{d/2 -m}.
\end{align}



Note that the gamma function $\Gamma(z)$ has simple poles at $z=0,-1,-2,\cdots,-n$. In that case, we should use the following approximation to solve the integral: \citep{Peskin:1995ev} 
\begin{align}
\Gamma(z)=\frac{1}{z} - \gamma + O(z),
\label{approxgamma}
\end{align}
where $\gamma\approx 0.5772$ is the Euler-Mascheroni constant. In case we have $m<0$, then the integral equals to zero.



Let us just cite some useful relations that we will use in the next section. From \cite{Peskin:1995ev}, we have \begin{align}
\int \frac{d^dl }{(2\pi)^d} \frac{l^2}{(l^2 -\Delta )^n} &= \frac{(-1)^{n-1} i }{(4\pi)^{d/2}} \frac{d}{2} \frac{\Gamma(n-\frac{d}{2}- 1)}{\Gamma(n)}\left(\frac{1}{\Delta}\right)^{n-\frac{d}{2}- 1},\label{reg1} \\
\int \frac{d^dl }{(2\pi)^d} \frac{l^\mu l^\nu}{(l^2 -\Delta )^n} &= \frac{(-1)^{n-1} i }{(4\pi)^{d/2}} \frac{g^{\mu \nu}}{2} \frac{\Gamma(n-\frac{d}{2}- 1)}{\Gamma(n)}\left(\frac{1}{\Delta}\right)^{n-\frac{d}{2}- 1},\label{reg2} \\
\int \frac{d^dl }{(2\pi)^d} \frac{(l^2)^2}{(l^2 -\Delta )^n} &= \frac{(-1)^{n} i }{(4\pi)^{d/2}} \frac{d(d+2)}{4} \frac{\Gamma(n-\frac{d}{2}- 2)}{\Gamma(n)}\left(\frac{1}{\Delta}\right)^{n-\frac{d}{2}- 2},\label{reg3} \\
\int \frac{d^dl }{(2\pi)^d} \frac{l^\mu l^\nu l^\rho l^\sigma}{(l^2 -\Delta )^n} &= \frac{(-1)^{n} i }{(4\pi)^{d/2}}  \frac{\Gamma(n-\frac{d}{2}- 2)}{\Gamma(n)}\left(\frac{1}{\Delta}\right)^{n-\frac{d}{2}- 2}\nonumber \\&\hspace{.125in} \times \frac{1}{4} \left( g^{\mu \nu}g^{\rho \sigma}+ g^{\mu \rho}g^{ \nu \sigma} + g^{\mu \sigma}g^{ \nu \rho}	\right). \label{noproofregularisation}
\end{align}

%\frac{d^dl }{(2\pi)^d} \frac{(l^\mu l^\nu l^\rho l^\sigma}{(l^2 -\Delta )^n} &= \frac{(-1)^{n} i }{(4\pi)^{d/2}}  \frac{\Gamma(n-\frac{d}{2}- 2)}{\Gamma(n)}\left(\frac{1}{\Delta}\right)^{n-\frac{d}{2}- 2}\times \frac{1}{4} \left( g^{\mu \nu}g^{\rho \sigma}+ g^{\mu \rho}g^{ \nu \sigma} + g^{\mu \sigma}g^{ \nu \rho}	\right).















% \caption{The area of a $d$-diemensional unit sphere for d equals to 1 to 4.}
%\end{figure}









