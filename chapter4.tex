 \chapter{The Higgs decays}
In the Lagrangian of the SM, a direct coupling between the Higgs and photons is impossible at tree level. As we know from Section \ref{fermionmass}, the coupling of the Higgs to other particles is proportional to their mass. Therefore, one may tend to think that the coupling of the  Higgs to photons is impossible as photons are massless. However, such a result is experimentally observed and it can happen at one-loop level (see Figure \ref{loop}). Mainly this process is affected by the $ W$-boson loop \citep{Chen:2006cs}, but this chapter will mainly focus on the contribution of the fermion loops. Note that additional two-loop QCD corrections and two-loop electroweak corrections exist for the $ H \to \gamma \gamma$ but these cases are much more complicated, and therefore will not be considered in this project \citep{Degrassi:2005mc}. After dealing with the $H\to \gamma \gamma $, we will briefly treat the case of $H$ to two gluons, since there are many similarities between the two processes. 
\begin{figure}[h!]\centering
\includegraphics[scale=0.45]{tyo}
\caption{Possible contributions for the decay to two photons.}
\label{loop}
\end{figure}
\section{ Higgs to two photons}
To begin with, let us compute the amplitude of transition of the process in Figure \ref{tloop} using the tools that we saw in Chapter \ref{chap3}, then compute its corresponding decay width.\begin{figure}[h]\centering
\includegraphics[scale=0.4]{test}
\caption{Higgs into two photons via fermions loop}
\label{tloop}
\end{figure} 
\newpage This is given by the following expression,
\begin{align}
\mathcal{M}_1&= - N_c \int \frac{d^4 q}{(2 \pi)^4} \left( -i \frac{m_f}{v} \right) i \frac{\slashed{q} - \slashed{p_2} + m_f}{ (q-p_2)^2 - m_f^2} \left( -i ee_q \gamma^\nu \right)  i \frac{\slashed{q} + m_f}{q^2 -m_f^2} \left( -i ee_q \gamma^\mu \right) i \frac{\slashed{q} + \slashed{p_1} + m_f}{ (q+p_1)^2 - m_f^2}\nonumber \\
 &\hspace{.125in} \epsilon^*_\mu (p_1,\lambda_1)\epsilon^*_\nu (p_2,\lambda_2).
\end{align}
By considering the result in Equation \eqref{massbosons}, we can write the above expression as follows:\begin{align}
\mathcal{M}_1&= -\frac{N_c e^3 e^2_q m_f}{2 \sin \theta_W m_W} \epsilon^*_\mu (p_1,\lambda_1)\epsilon^*_\nu (p_2,\lambda_2) \int  \frac{d^4 q}{(2 \pi)^4} \frac{Tr[(\slashed{q} - \slashed{p_2} + m_f)\gamma^\nu (\slashed{q} + m_f) \gamma^\mu(\slashed{q} + \slashed{p_1} + m_f)]}{[(q-p_2)^2 - m_f^2] [q^2 -m_f^2] [(q+p_1)^2 - m_f^2]},
\end{align}
where $ N_c $ is the colour factor, which is equal to 3 for quarks (SU(3) gauge theory) and 1 for leptons. $ e_q$ is the charge of the quark in units of the electron charge $e$, and the minus sign is due to the rule that any fermion loop must be multiplied by -1. The angle $\theta_W$ is the Weinberg angle defined in Equation \eqref{Weinbergangle}, $m_W$ the mass of the boson $W^\pm$.
It is good to remind ourselves that the colour charge does not change \citep{Peskin:1995ev}. The reason for that is because the photon and the Higgs do not interact strongly and each vertex conserves colour charge.

Notice that if we expand the numerator, all the terms proportional to $q^3$ will vanish as the trace of an odd number of gamma matrices is equal to zero (see \eqref{Trace2}), and the terms proportional to $q^2$ are logarithmically divergent. In that situation we can tackle the issue by using the dimensional regularisation technique for $4 \longrightarrow d$. To start the computation, let us first of all compute the numerator by using the trace properties in Appendix \ref{sectapp}:\begin{align}
&Tr[ (\slashed{q} - \slashed{p_2} + m_f)\gamma^\nu (\slashed{q} + m_f) \gamma^\mu(\slashed{q} + \slashed{p_1} + m_f) ] \nonumber \\&= 16 q^\mu q^\nu m_f - 8 q^\mu p_2^\nu m_f + 8 q^\nu p_1^\mu m_f - 4 p_1^\mu p_2^\nu m_f + 4 p_1^\nu p_2^\mu +4 g^{\mu \nu} m^3_t - 4 g^{\mu \nu } q^2 m_f - 4g^{\mu \nu } (p_1 \cdot p_2) m_f \nonumber \\
&=N^{\mu \nu}.
\end{align}
The amplitude matrix can be written as follows,
\begin{align}
\mathcal{M}_1 = -\frac{N_c e^3 e^2_q m_f}{2 \sin \theta_W m_W} \epsilon^*_\mu (p_1,\lambda_1)\epsilon^*_\nu (p_2,\lambda_2)  \int  \frac{d^d q}{(2 \pi)^d} \frac{N^{\mu \nu}}{D_1 D_2 D_3},
\label{ampl}
\end{align}
where $ D_1 = q^2 -m_f^2 $, $D_2=(q-p_2)^2 - m_f^2$, $D_3=(q+p_1)^2 - m_f^2 $. In order to solve this integral, let us apply the Feynman parametrisation that we saw in Equation \eqref{FP}. We get:
\begin{align}
\frac{1}{D_1 D_2 D_3}= \int_0^x dx  \int_0^{1-x}  dy \frac{2}{[D_1(1-x-y) +D_2x +D_3y]^3}.
\label{feynmparam} 
\end{align}
The idea is to now shift the variable $q$, but let us expand the denominator first,
\begin{align}
D&= (q^2 -m_f^2 ) (1-x-y) + [ (q-p_2)^2 - m_f^2]x + [(q+p_1)^2 - m_f^2]y \nonumber \\
&=q^2 -m_f^2 - 2qp_2x+ 2qp_1 y ,~~~(p_1^2=p_2^2=0). \label{D}
\end{align} By shifting $q$ we can use a new variable $l$ such that $l= q - p_2x +yp_1.$ This choice will lead to
\begin{align}
 l^2 + 2p_2p_1 xy= q^2 - 2p_2xq + 2yp_1q .
 \label{shift}
\end{align} 
Inserting \eqref{shift} into \eqref{D} we get \begin{align}
D=  l^2 + 2p_2p_1 xy-m_f^2. \label{Dnew}
\end{align}
From Equation \eqref{Dnew} we can conclude that $ d^4q=d^4l$, and now the integral \eqref{feynmparam} becomes: \begin{align}
2 \int_0^1 dx \int_0^{1-x} dy [D_1(1-x-y) +D_2x +D_3y]^{-3} = 2  \int_0^1 dx \int_0^{1-x} dy [l^2 - \Delta ]^{-3},
\end{align}
where $ \Delta =m_f^2- 2p_2p_1 xy$.

Let us express the numerator with respect to the new variable $l$. However, the numerator $N^{\mu \nu}$ can be simplified by using the property of photons. That is, we know that a longitudinal polarisation is not allowed for the photons. Mathematically, it can be written as follows $~~ p_1^\mu \epsilon^*_\mu(p_1,\lambda_1)= p_2^\nu \epsilon^*_\mu(p_2,\lambda_2)=0$. We will then be left with:

\begin{align}
Tr[ \slashed{q} - (\slashed{p_2} + m_f)\gamma^\nu (\slashed{q} + m_f) \gamma^\mu(\slashed{q} + \slashed{p_1} + m_f) ] =  4m_f \left[  4 q^\mu q^\nu + p_1^\nu p_2^\mu - g^{\mu \nu} \left( q^2 - m_f^2 +(p_1 \cdot p_2)  \right)  \right].
\end{align}
By using the relation in Equation \eqref{shift}, we obtain:
\begin{align}
N'^{\mu \nu}\nonumber = &4m_f \left[ 4 l^\mu l^\nu- 4l^\mu p_1^\nu y +  4l^\nu p_2^\mu x -4 p_1^\nu p_2^\mu xy +p_1^\nu p_2^\mu \right. \\
&- \left. g^{\mu \nu} \left( l^2 +2(l \cdot p_2)x - 2 (l\cdot p_1)y - 2xy(p_1\cdot p_2) -m_f^2 + (p_1 \cdot p_2) \right) \right].
\end{align}
As such the matrix element becomes
\begin{align}
\mathcal{M}_1 &= -\frac{4N_c e^3 e^2_q m_f^2}{ \sin \theta_W m_W} \epsilon^*_\mu (p_1,\lambda_1)\epsilon^*_\nu (p_2,\lambda_2)  \int  \frac{d^d l}{(2 \pi)^d}  \int_0^1 dx \int_0^{1-x} dy [l^2 - \Delta ]^{-3}\nonumber N'^{\mu \nu} .
\end{align} 
%\\ & \times  \left[ 4 l^\mu l^\nu- 4l^\mu p_1^\nu y +  4l^\nu p_2^\mu x -4 p_1^\nu p_2^\mu xy +p_1^\nu p_2^\mu - g^{\mu \nu} \left( l^2 +2l\cdot p_2x - 2 l\cdot p_1y - 2xy(p_1 \cdot p_2) -m_f^2 + (p_1 \cdot p_2) \right) \right]
All the terms proportional to $l$ will vanish as it is antisymmetric under $l \to -l$, and we are integrating over all $l$. Therefore, we will only have to compute the terms with even power of $l$ in the numerator. We then have
\begin{align}
\mathcal{M}_1 &= -\frac{4N_c e^3 e^2_q m_f^2}{ \sin \theta_W m_W} \epsilon^*_\mu (p_1,\lambda_1)\epsilon^*_\nu (p_2,\lambda_2)   \int  \frac{d^d l}{(2 \pi)^d}  \int_0^1 dx \int_0^{1-x} dy [l^2 - \Delta ]^{-3}\nonumber \\   & \hspace{.125in} \times  \left[ 4 l^\mu l^\nu -4 p_1^\nu p_2^\mu xy +p_1^\nu p_2^\mu - g^{\mu \nu} \left( l^2 - 2xy(p_1 \cdot p_2) -m_f^2 + (p_1 \cdot p_2) \right) \right] .
\end{align} 
Let us apply the relations \eqref{reg1} and \eqref{reg2} from the previous chapter to compute the terms proportional to $l^2$ and $l^\mu l^\nu$, 
%Applying what we saw in the regularisation section, we are allowed to write the term proportional to $l^2$ as follow:
\begin{align}
&\int \frac{d^d l}{(2 \pi)^d}\frac{4 l^\mu \l^\nu }{[l^2 - \Delta ]^{3}}- \int \frac{d^d l}{(2 \pi)^d}\frac{ g^{\mu \nu} l^2}{[l^2 - \Delta ]^{3}}\nonumber \\
&= 4 \frac{(-1)^2 i}{(4\pi)^{d/2}} \frac{g^{\mu \nu}}{2}\frac{\Gamma(2-d/2)}{\Gamma (3)}  \left(\frac{1}{\Delta}\right)^{2-d/2} - g^{\mu \nu} \frac{(-1)^2 i}{(4\pi)^{d/2}} \frac{d}{2} \frac{\Gamma(2-d/2)}{\Gamma (3)}\left(\frac{1}{\Delta}\right)^{2-d/2} ,\nonumber \\
&=  \frac{ i g^{\mu \nu}}{2 (4\pi)^{d/2}} \frac{\Gamma(2-d/2)}{\Gamma (3)}\left(\frac{1}{\Delta}\right)^{2-d/2} (4 -d ).
\label{Ma}
\end{align}
We notice that this expression has poles at $d=4$, so we should expand $\Gamma(2-d/2)$ according to Equation \eqref{approxgamma} and define $d=4-2\epsilon$. 

In the limit $d\to4$, which means $\epsilon \to 0$, we have \begin{align}
\Gamma(\epsilon) = \left( \frac{1}{\epsilon} -\gamma + O(\epsilon) \right),
\label{expans1}
\end{align}
where $\gamma= 0.5772 $ is the Euler-Mascheroni constant. Also, we have: 
\begin{align}
\left(\frac{1}{\Delta}\right)^{2-d/2} = \left( 1 - \epsilon \log \Delta  + \ldots \right).
\label{expans2}
\end{align} 
However, the latter is not correct as the $\log	$ is not dimensionless. For this reason we need to introduce a parameter $\mu^{4-d}$, with the same dimension as $\Delta$, so that the logarithm appearing in the expansion will be dimensionless. We have then:
\begin{align}
\Delta^{d/2-2}=\Delta^{-\epsilon}\cdot \mu^{2\epsilon}\cdot \mu^{-2\epsilon}.
\end{align} 
The expansion then becomes 
\begin{align}
\left( \frac{\Delta}{\mu^2} \right)^{-\epsilon} = 1 - \epsilon \log \frac{\Delta}{\mu^2} + O(\epsilon^2).
\end{align}
The factor $\mu^{4-d}=\mu^{2\epsilon}$ that we introduced will be equal to one when we set $d\to4$.
%\mu^{\epsilon} \times \frac{1}{\Delta}^\epsilon = e^{\epsilon \log \frac{\mu^2 }{\Delta} = 1- \epsilon \log	 \frac{\mu^2 }{\Delta}.
Hence, we obtain \begin{align}
&\int \frac{d^d l}{(2 \pi)^d}\frac{4 l^\mu \l^\nu }{[l^2 - \Delta ]^{3}}- \int \frac{d^d l}{(2 \pi)^d}\frac{ g^{\mu \nu} l^2}{[l^2 - \Delta ]^{3}}=\frac{ i g^{\mu \nu}}{2 (4\pi)^{\epsilon -2}} \epsilon \Gamma(\epsilon) \left(\frac{1}{\Delta}\right)^{\epsilon}.
\end{align}
%&= \frac{ i g^{\mu \nu}}{2 (4\pi)^{\epsilon -2}} \epsilon \Gamma(\epsilon) \left(\frac{1}{\Delta}\right)^{\epsilon}.

%%%%%%
%As we choose , and we take the limit $d \longrightarrow 4$, we get $\epsilon \longrightarrow 0$. Thus we are in one of the poles of the gamma function ( poles for n=0,-1,-2,...). To tackle this issue, we expand the gamma function near 0, that is:
%\begin{align}
%\Gamma(\epsilon) = \left( \frac{1}{\epsilon} - \gamma + o(\epsilon) \right) . 
%\label{expans1}
%\end{align}



Combining \eqref{expans1} and \eqref{expans2} into \eqref{Ma}, we get:
\begin{align}
\frac{i g^{\mu \nu}}{ 2(4 \pi)^2}  \epsilon \left( \frac{1}{\epsilon} - \gamma + O(\epsilon) \right)= \frac{i g^{\mu \nu}}{ 2(4 \pi)^2} + O(\epsilon). \label{q2}
\end{align}
It is now time to compute all the remaining terms. Let us call $C^{\mu \nu}$ the terms in the numerator to get
\begin{align}
&\int \frac{d^d l}{(2 \pi)^d}  \frac{ -4xy p_1^\nu p_2^\mu +  p_1^\nu p_2^\mu -g^{\mu \nu} \left(  -2xy(p_1 \cdot p_2) -m_f^2 +(p_1\cdot p_2 ) \right)}{[l^2 - \Delta ]^{3} }\nonumber \\
&=C^{\mu \nu } \int \frac{d^d l }{(2\pi)^d}[l^2 - \Delta ]^{3} \nonumber  \\
&=C^{\mu \nu} \frac{(-1)^3 i }{(4\pi)^{d/2}} \frac{\Gamma(3-d/2)}{\Gamma(3)} \frac{1}{\Delta}^{3-d/2}\\
&= -\frac{i}{2(4\pi)^2 \Delta}C^{\mu \nu } ~~~({d\longrightarrow 4}).
\label{q0} 
 \end{align}

By adding the relation \eqref{q2} and \eqref{q0} together, we have\begin{align}
i \frac{g^{\mu \nu }\Delta-C^{\mu \nu}}{2(4\pi)^2 \Delta}= \frac{i}{2(4\pi)^2 \Delta}\left(  p_1^\nu p_2^\mu - g^{\mu \nu} (p_1\cdot p_2) \right) \left( 4xy - 1 \right).
\end{align}
Hence, the matrix element is 
\begin{align}
\mathcal{M}_1= -\frac{2 N_c e^3 e_q^2 m_f^2}{\sin \theta_W m_W} \epsilon^*_\mu (p_1,\lambda_1)\epsilon^*_\nu (p_2,\lambda_2)\int_0^1 dx \int_0^{1-x} dy  \frac{i}{2(4\pi)^2 \Delta}\left(  p_1^\nu p_2^\mu - g^{\mu \nu} (p_1 \cdot p_2) \right) \left( 4xy - 1 \right).\label{M}
\end{align}

The four momentum conservation allows us to write $  m_H^2 = p_H^2 = (p_1+ p_2)^2 = 2p_1 \cdot p_2 $ for photons with $ p_1^2 =p_2^2=0$. Therefore \begin{align}
\Delta= m_f^2 - 2xy p_1 \cdot p_2= m_f^2 \left( 1- xy\frac{ m_H^2}{ m_f^2}\right),
\end{align}
and the matrix element can be rewritten as 
%\begin{align}
%\mathcal{M}_1 = \frac{2i N_c e^2 e_q^3 m_f^2}{(4\pi)^2 \sin \theta_W m_W} \epsilon^*_\mu (p_1,\lambda_1)\epsilon^*_\nu (p_2,\lambda_2) \left(  p_1^\nu p_2^\mu - g^{\mu \nu} \frac{m_H^2}{2} %\right) \times \int_0^1 dx \int_0^{1-x} dy \frac{1-4xy }{1- xy \frac{m_H^2}{m_f^2}},
%\end{align}
\begin{align}
\mathcal{M}_1 = \frac{i N_c e^3 e_q^2 }{ (4\pi)^2 \sin \theta_W m_W} \epsilon^*_\mu (p_1,\lambda_1)\epsilon^*_\nu (p_2,\lambda_2) I\left(\frac{m_H^2}{m_f^2}\right) \left(  p_2^\mu p_1^\nu -  (p_1 \cdot p_2) g^{\mu \nu}  \right), \label{finalmat}
\end{align}
where  \begin{align}
I\left(\frac{m_H^2}{m_f^2}\right) &= \int_0^1 dx \int_0^{1-x} dy \frac{1-4xy }{1- xy \frac{m_H^2}{m_f^2}}. \label{Formfact}
\end{align}
This integral \eqref{Formfact} is a form factor. It can be seen in \cite{Bergstrom:1985hp}'s article,
\begin{align}
I\bigg(\frac{m_H^2}{m_t^2}\bigg) =\frac{1}{2} \left[  \alpha_f + (\alpha_f -1)f(\alpha_f)  \right]\alpha_f^{-2},  \label{I}
\end{align}
%\frac{1}{2\alpha_f} \left\lbrace 1 + \frac{(\alpha_f-1)}{\alpha_f} f(\alpha_f)  \right\rbrace
such that 
\begin{align}
f(\alpha_f)  = \begin{cases}
\arcsin^2(\sqrt{\alpha_f}),~~~~~~~~~~~~~&0<\alpha_f<1\\
-\log^2( \sqrt{\alpha_f}+ \sqrt{\alpha_f - 1}) + \frac{1}{4} \pi^2 + i \pi \log(\sqrt{\alpha_f}+ \sqrt{\alpha_f - 1}), ~~~~~~~~~~~~~&\alpha_f>1, 
\end{cases}
\end{align}
and $\alpha_f= \frac{m_H^2}{4m_f^2}$.
What we have here is the amplitude for the first diagram. For the second diagram, we are not going to do all the computation. We can just exchange the momenta in \eqref{finalmat} as $ (  p_1,\lambda_1 ) \longleftrightarrow (p_2,\lambda_2 )$. Then we get the matrix element for the second diagram. However, the result will be the same as the one in the first diagram. Hence, the total matrix element for the Higgs to diphotons is: 
\begin{align}
\mathcal{M} &= \mathcal{M}_1 + \mathcal{M}_2 = 2 \mathcal{M}_1\nonumber \\
&= \frac{i N_c e^3 e_q^2}{8 \pi^2 \sin \theta_W m_W} \epsilon^*_\mu (p_1,\lambda_1)\epsilon^*_\nu (p_2,\lambda_2) \left(  p_1^\nu p_2^\mu - g^{\mu \nu} \frac{m_H^2}{m_f^2} \right) I\bigg(\frac{m_H^2}{m_f^2}\bigg).
\end{align}
The plot of \eqref{Formfact} shows us that the top quark is the only important process for the loop. In fact, we see in Figure \ref{formfactfig} that when $ \alpha_f \to 0 $ and $\alpha_f \to \infty$, the value of the form factor goes quickly to its limiting values. That is, all the light quarks will not give us a good approximation of the Higgs mass, as the form factor in Equation \eqref{I} is proportional to the fermion mass squared $m_f^2$. That result is in accordance with the LHC experiments \citep{Kotsky:1997rq} and the theory developed in Chapter \ref{chapter2} that the Higgs couples proportionally to the fermion mass. Therefore, we will take $ \alpha_f\to\alpha_t= \frac{m_H^2}{4m_t^2} < 1 $ (\cite{Aad:2012tfa} and \cite{Chatrchyan:2012xdj}) in the case of the $H\to\gamma\gamma$. 
\begin{figure}[h!] \centering
\includegraphics[scale=0.4]{formfactor}
\caption{The form factor as a function of the Higgs mass}
\label{formfactfig}
\end{figure}
We can now take the square of the matrix and sum over the possible polarisations \begin{align}
\sum_{\lambda_1,\lambda_2} \epsilon^*_\mu (p_1,\lambda_1) \epsilon^*_\nu(p_1,\lambda_1)  \epsilon_\rho (p_1,\lambda_1)\epsilon_\sigma(p_1,\lambda_1)= g_{\mu \rho} g_{ \nu \sigma}.
\end{align}
Then, we get: \begin{align}
\mathcal{M}^2 = \frac{  N_c^2 \alpha^3 e_q^4 m_H^4}{2\pi \sin^2 \theta_W m^2_W} \bigg|I\bigg(\frac{m_H^2}{m_t^2}\bigg)\bigg|^2 ,
\end{align} 
where we used the relation \begin{align}
e=\sqrt{4 \pi \alpha} \Rightarrow e^6 = 64 \pi^3 \alpha^3,
\end{align}
and \begin{align}
I\bigg(\frac{m_H^2}{m_t^2}\bigg)= \frac{1}{2}[ \alpha_t + (\alpha_t-1) \arcsin^2 \sqrt{\alpha_t} ] \alpha_t^{-2} .
\label{A}
\end{align}
As said earlier, this computation takes only the fermion loops into consideration.
\subsection{The decay rate of the process}
For a particle of mass $m$ decaying into two massless particles, the decay rate can be written as \citep{Griffiths:1987tj}:
\begin{align}
\Gamma = \frac{S}{16 \pi m_H}  |\mathcal{M} |^2 .
\end{align}
In this process the factor $S=\frac{1}{2}$ because we have two identical outgoing particles, thus we get: 
\begin{align}
\Gamma(H \longrightarrow \gamma \gamma ) = \frac{N_c^2 \alpha^3 m_H^3 e_q^2}{64 \pi^2 \sin^2 \theta_W m^2_W} \bigg|I\bigg(\frac{m_H^2}{m_t^2}\bigg)\bigg|^2 .
\end{align}












% $I( \alpha_f)$












%Some research projects may have 3, 5 or 6 chapters. This is just an example. 
%More importantly, do you have at close to 30 pages?  
%Luck has nothing to do with it. Use the techniques suggested for
%writing your research project.

%Now you're demonstrating pure talent and newly acquired skills. 
%Perhaps some persistence. Definitely some inspiration. What was that about perspiration? 
%Some team work helps, so every now and then why not browse your friends' research project and provide
%some constructive feedback?

%

\section{Higgs to two gluons}
In this section we are going to compute the decay width of the process $H\to gg $. The latter is very similar to the $H\to \gamma \gamma$ and we still have to consider the fermion-loop. This is because gluons are massless and do not couple directly to the Higgs. Furthermore, no possible interaction can occur between gluons and $W$'s, so the following diagrams are the entire one-loop level calculation for this process.
%This case is very similar to the previous one, so we will not do explicitly the computation again. However, because gluons interact strongly\footnote{That is also another reason we have one loop process as the Higgs do not interact strongly, thus a direct coupling at tree level does not exist. Furthermore, like photons, gluons are massless.}, so $W$-loops are not possible. Therefore, the entire one loop level calculation is expressed by the following diagrams:
%one has to take account of the colour charges in the computation. The diagram for the process is given by the following figure,
\begin{figure}[h!]\centering
\includegraphics[scale=0.3]{hggnew}
\caption{Diagrams contributing for the one loop process $ H\to gg.$}
\end{figure} 
\newline As gluons interact strongly, one has to take into account their colour charges during the process.  That is, the amplitude of the first diagram can be written as follows:
\begin{align}
\mathcal{M}_1&= -i \frac{m_t}{v} (-1) \int \frac{d^4 q}{(2\pi)^4} \left( i \frac{\slashed{q}-\slashed{p_2}+m_t}{(q-p_2)^2-m_t^2}\right) (-ig_s \gamma^\nu (t^b)_{lk'}) \left( i \frac{\slashed{q}+m_t}{q^2-m_t^2} \right)  (-ig_s \gamma^\mu (t^a)_{j'k'}) \nonumber \\ &\hspace{.125in} \left( i \frac{\slashed{q}+\slashed{p_1}+m_t}{(q+p_1)^2-m_t^2}\right) \epsilon^{*a}_{\mu , r_1}\epsilon^{*a}_{\nu , r_2} \delta_{kk'}\delta_{j'i}\delta_{il} \nonumber \\
&= - \frac{m_t}{v} g_s^2 \epsilon^{*a}_{\mu , r_1}\epsilon^{*a}_{\nu , r_2} \delta_{kk'}\delta_{j'i}\delta_{il}  (t^a)_{j'k'} (t^b)_{lk'}\nonumber \\  &\hspace{.125in} \times \int \frac{d^4 q}{(2\pi)^4} \frac{Tr [ (\slashed{q}-\slashed{p_2}+m_t) \gamma^\nu (\slashed{q}+m_t)\gamma^\mu (\slashed{q}+\slashed{p_1}+m_t ) ] }{ [(q-p_2)^2-m_t^2)( q^2-m_t^2) ((q+p_1)^2-m_t^2]}.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\end{align}
By computing the colour trace \begin{align}
(t^a)_{j'k'} (t^b)_{lk'} \delta_{kk'}\delta_{j'i}\delta_{il}   =Tr[ t^a  t^b]= \frac{1}{2}\delta^{ab} ,
\end{align}
we obtain:
\begin{align}
\mathcal{M}_1 = \frac{ 2 g_s^2 e}{(4\pi)^2 m_W \sin \theta_W} \epsilon^{*a}_{\mu,r_1} (p_1) \epsilon^{*b}_{\nu,r_2} (p_2) \delta^{ab} I\bigg(\frac{m_H^2}{m_t^2}\bigg) \left(  p_2^\mu p_1^\nu -  (p_1 \cdot p_2) g^{\mu \nu}  \right) ,
\label{finalmat}
\end{align}
with $I(\frac{m_H^2}{m_f^2}) $ as defined in Equation \eqref{A}.
Like the $ H \to \gamma \gamma$ case, the contribution of the second diagram is the same as the first, so the total amplitude for the process is 
\begin{align}
\mathcal{M} &= 2\mathcal{M}_1 \\
&= \frac{ 4 g_s^2 e}{(4\pi)^2 m_W \sin \theta_W} \epsilon^{*a}_{\mu,r_1} (p_1) \epsilon^{*b}_{\nu,r_2} (p_2) \delta^{ab} I\bigg(\frac{m_H^2}{m_t^2}\bigg) \left(  p_2^\mu p_1^\nu -  (p_1 \cdot p_2) g^{\mu \nu}  \right).
\label{Mgg}
\end{align}
%\frac{1}{2} \frac{ g_s^2 m_f}{(4\pi)^2 v} \epsilon^{*a}_{\mu, r_1} \epsilon^{*b}_{\nu, r_2} \delta^{ab} A^\gamma( \alpha_f) \left(  p_2^\mu p_1^\nu -  (p_1 \cdot p_2) g^{\mu \nu}  \right)
%For the second diagram, we have the same amplitude. Therefore, the total amplitude is given by \begin{align}
%\mathcal{M}&=2\mathcal{M}_1 \nonumber \\ 
%\end{align}
Let us take the square of Equation \eqref{Mgg} and then compute the decay width of the process. For that we have to sum over the spins and the colours.\begin{align}
&\sum_{a,b} \delta^{ab} \delta^{ab} =8, \\
&\sum_{r_1,r_2} \epsilon^{*a}_{\rho,r_1}  \epsilon^{*a}_{\mu, r_1} \epsilon^{*b}_{\sigma,r_2} \epsilon^{*b}_{\nu, r_2}= g_{\mu \rho} g_{\sigma \nu},
\end{align} 
to get the final expression:\begin{align}
|\mathcal{M}|^2 = \frac{e^2 m_H^4 g_s^4}{4 \pi^4 m_W^2 \sin^2 \theta_W}\bigg| I\bigg(\frac{m_H^2}{m_t^2}\bigg)\bigg|^2.
\end{align}
%with $A^g(\alpha_)$ is the same as in the equation \eqref{A}.

\subsection{The decay width}
%Let us just apply the formula we have in the section ??. \footnote{Notice that the symmetry factor is one as we do not get identical outgoing  particles.}
It is given by 
\begin{align}
\Gamma = \frac{S}{16 \pi m_H}  |\mathcal{M} |^2 .
\end{align}

By plugging the amplitude into this expression, we obtain: 
\begin{align}
\Gamma= \frac{\alpha  \alpha_s^2 m_H^3}{ 4\pi^2 m_W^2 \sin^2 \theta_W} \bigg|  I\bigg(\frac{m_H^2}{m_t^2}\bigg) \bigg|^2.
\end{align}

As we said earlier, at one loop level this is the full result for the $ H \to gg $, but for the $ H\to \gamma \gamma$ we have to add the contributions for $W$-loops. 

%By plugging the amplitude matrix in this expression, we obtain
%\begin{equation}
%\mathcal{M}= -\frac {e \cdot g_s^2 }{2m_W \sin \theta_W} \cdot \frac{1}{2} \delta^{ab} \frac{ \frac{1}{2} m^2_H \cdot  g^{\mu \nu} - p^\nu k^\mu }{4 \pi^2 } \cdot I(\alpha_f) \times \epsilon^{a*}_{\mu, r_1} \epsilon^{b*}_{\nu, r_2}
%\end{equation} 


%An average research project may contain five chapters, but I didn't plan my work properly
%and then ran out of time. I spent too much time positioning my figures and worrying
%about my preferred typographic style, rather than just using what was provided.
%I wasted days bolding section headings and using double slash line endings, and 
%had to remove them all again. I spent sleepless nights configuring manually numbered lists
%to use the \LaTeX\ environments because I didn't use them from the start or understand
%how to search and replace easily with texmaker.

%Everyone has to take some shortcuts
%at some point to meet deadlines. Time did not allow to test model 
%B as well. So I'll skip right ahead and put that under my Future Work section.




%\begin{huge}
%Need to correct this section(the argument).
%\end{huge}
%$H \to \gamma $
%As we can see in the Lagrangian of the SM, the coupling between the Higgs and photons does not occur at tree-level. However, such a result is experimentally observed. How could that be possible? Actually, at one loop level, the decay of the Higgs into two photons can take place, either via fermion loops or via  W-boson loops. In this chapter, we are going to focus on the case where the process is produced via a top quark loop. Even if this could be possible via all fermions, it is better to point that the higher the mass of the fermion, the higher is the probability. That is why the reason we consider only the top quark in our quest (top quark mass $m_f \sim 173.34 Gev$.)

%\mathcal{M}_1 =  g_s^2 \frac{m}{v} \epsilon^{a*}_{\mu, r_1} \epsilon^{b*}_{\nu,r_2} Tr [ \lambda^a \lambda^b ] \int	 \frac{Tr [ \gamma^\mu \left( \slashed{q}+\slashed{p_1}+m \right) \left(  \slashed{q}-\slashed{p_2}+m  \right) \left( \slashed{q} +m \right)]}{ \left( q^2-m^2 \right)  \left( (q+p_2)^2-m^2 \right)  \left(  (q+p_1)^2-m^2 \right) }
%newpage %In this section, we are going to compute the decay width of the process $ H \longrightarrow gg$. 
%The latter is very similar to the $ H \longrightarrow$ $\gamma$ $\gamma$ and we still have to consider one fermion-loop. That is because the gluons are massless and do not couple directly to the Higgs as the Higgs does not interact strongly. Thus in this process, we have to take account of the colour charges of the gluons.

 %\begin{align}
%and applying the relations in \eqref{gande} and \eqref{massbosons}
%\mathcal{M}_1 &= -\frac{ e g_s^2 m_f^2}{sin \theta_W m_W} \epsilon^{*a}_\mu (p_1)\epsilon^{b*}_\nu (p_2) \delta^{ab} \mu^{4-d}  \int  \frac{d^d l}{(2 \pi)^d}  \int_0^1 dx \int_0^{1-x} dy [l^2 - \Delta ]^{-3}\nonumber \\ & \times  \left[ 4 l^\mu l^\nu- 4l^\mu p_1^\nu y +  4l^\nu p_2^\mu x -4 p_1^\nu p_2^\mu xy +p_1^\nu p_2^\mu - g^{\mu \nu} \left( l^2 +2lp_2x - 2 lp_1y - 2xyp_1p_2 -m_f^2 + p_1p_2 \right) \right],
%\end{align}

%&= \frac{1}{4} \left[ \left( \alpha_f + (\alpha_f -1) \arcsin^2 \sqrt{\alpha_f}  \right) \right]\alpha_f^{-2}\nonumber \\&= \frac{1}{4 } A ( \alpha_f)~~~ \text{and}~~~\alpha_f=\frac{m_H^2}{4m_f^2}. 
%Finally, the matrix element becomes


%After doing a long of computation(see Appendix ), one ends up with the following expression.
%\begin{align}
%\mathcal{M}_1 = \frac{N_c Q_t^2 m_f}{(4\pi)^2v} \epsilon^*_\mu (p_1,\lambda_1)\epsilon^*_\nu (p_2,\lambda_2) I( \alpha_f) \left(  p_2^\mu p_1^\nu -  ((p_1 \cdot p_2)) g^{\mu \nu}  \right) \label{finalmat}
%\end{align}
%where $I(\alpha_f) = \int_{0}^1 dx \int_0^{1-x} dy \frac{1-4xy}{1-4xy\alpha_f} , ~~~\alpha_f= \frac{m_H^2}{m_f^2}$.
%\begin{align}
%&Tr[ \slashed{q} - (\slashed{p_2} + m_f)\gamma^\nu (\slashed{q} + m_f) \gamma^\mu(\slashed{q} + \slashed{p_1} + m_f) ]\nonumber \\&= 16 q^\mu q^\nu m_f - 8 q^\mu p_2^\nu m_f + 8 q^\nu p_1^\mu m_f - 4 p_1^\mu p_2^\nu m_f + 4 p_1^\nu p_2^\mu +4 g^{\mu \nu} m^3_t - 4 g^{\mu \nu } q^2 m_f - 4g^{\mu \nu } p_1 p_2 m_f. 
%\end{align}


%, this expression can be simplified by using the property of photons. We know that a longitudinal polarisation is not allowed for the photons. Mathematically, it can be written as follows $~~ p_1^\mu \epsilon^*_\mu(p_1,\lambda_1)= p_2^\nu \epsilon^*_\mu(p_2,\lambda_2)=0$. We will be left then with: